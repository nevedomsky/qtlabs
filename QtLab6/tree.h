#ifndef TREE_H
#define TREE_H

#include <QDebug>
#include <queue>
#include <vector>
#include <tuple>
#include <utility>

template<typename E>
class Node
{
public:
    E data;
    int key;
    Node* left = nullptr;
    Node* right = nullptr;

    Node(int itemkey, E item) : key(itemkey), data(item) {}
};

template<typename E>
class TreeIterator
{
private:
    std::queue<Node<E>*> q;

    static void _inorder(Node<E>* from, std::queue<Node<E>*> &queue)
    {
        if (from->left != nullptr) _inorder(from->left, queue);
        queue.push(from);
        if (from->right != nullptr) _inorder(from->right, queue);
    }

    static void _preorder(Node<E>* from, std::queue<Node<E>*> &queue)
    {
        queue.push(from);
        if (from->left != nullptr) _inorder(from->left, queue);
        if (from->right != nullptr) _inorder(from->right, queue);
    }

    static void _postorder(Node<E>* from, std::queue<Node<E>*> &queue)
    {
        if (from->left != nullptr) _inorder(from->left, queue);
        if (from->right != nullptr) _inorder(from->right, queue);
        queue.push(from);
    }

    TreeIterator(std::queue<Node<E>*> &q)
    {
        this->q = q;
    }

public:
    static TreeIterator inorder(Node<E>* root)
    {
        std::queue<Node<E>*> q;
        _inorder(root, q);
        return TreeIterator(q);
    }

    static TreeIterator preorder(Node<E>* root)
    {
        std::queue<Node<E>*> q;
        _preorder(root, q);
        return TreeIterator(q);
    }

    static TreeIterator postorder(Node<E>* root)
    {
        std::queue<Node<E>*> q;
        _postorder(root, q);
        return TreeIterator(q);
    }

    bool hasNext()
    {
        return !q.empty();
    }

    E next()
    {
        return nextRaw()->data;
    }

    Node<E>* nextRaw()
    {
        auto node = q.front();
        q.pop();
        return node;
    }
};

template <typename E>
class Tree
{
private:
    Node<E>* root;

    int m_size;

    void _add(Node<E>* where, Node<E>* what)
    {
        if (what->key == where->key)
        {
            where->data = what->data;
        }
        else
        {
            if (what->key < where->key)
            {
                if (where->left != nullptr)
                    _add(where->left, what);
                else
                {
                    where->left = what;
                    m_size++;
                }
            }
            else if (what->key > where->key)
            {
                if (where->right != nullptr)
                    _add(where->right, what);
                else
                {
                    where->right = what;
                    m_size++;
                }
            }
        }
    }

    void _print2(Node<E>* node)
    {
        if (node->left != nullptr) _print(node->left);
        qDebug() << node->data;
        if (node->right != nullptr) _print(node->right);
    }

    void _print()
    {
        std::queue<Node<E>*> q;
        q.push(root);
        q.push(nullptr);
        QString s;

        while (q.size() > 1)
        {
            auto node = q.front();
            if (node == nullptr)
            {
                qDebug() << s;
                s.clear();
                q.pop();
                q.push(node);
                continue;
            }

            s.append(" ").append(QString::number(node->key)).append(" ");

            if (node->left != nullptr) q.push(node->left);
            if (node->right != nullptr) q.push(node->right);

            q.pop();
        }

        qDebug() << s;
    }

    void _tree_to_list(std::vector<Node<E>*> &v, Node<E>* root)
    {
        if (root->left != nullptr) _tree_to_list(v, root->left);

        v.push_back(root);

        if (root->right != nullptr) _tree_to_list(v, root->right);
    }

    Node<E>* _list_to_balanced_tree(std::vector<Node<E>*> &v, int from, int to)
    {
        if (from > to) return nullptr;

        int mid = (from + to) / 2;

        auto node = v[mid];

        node->left = _list_to_balanced_tree(v, from, mid - 1);
        node->right = _list_to_balanced_tree(v, mid + 1, to);

        return node;
    }

    Node<E>* _deleteNode(Node<E>* root, int key)
    {
        if (root == nullptr) return root;

        if (key < root->key)
            root->left = _deleteNode(root->left, key);

        else if (key > root->key)
            root->right = _deleteNode(root->right, key);

        else
        {
            m_size--;
            if (root->left == nullptr)
            {
                Node<E>* temp = root->right;
                delete root;
                return temp;
            }
            else if (root->right == nullptr)
            {
                Node<E>* temp = root->left;
                delete root;
                return temp;
            }

            Node<E>* current = root->right;
            while (current->left != NULL)
                current = current->left;

            root->key = current->key;

            root->right = _deleteNode(root->right, current->key);
        }

        return root;
    }

    Node<E>* _get(Node<E>* where, int &key)
    {
        if (where == nullptr) return where;

        if (key == where->key)
        {
            return where;
        }
        else
        {
            if (key < where->key)
            {
                return _get(where->left, key);
            }
            else if (key > where->key)
            {
                return _get(where->right, key);
            }
        }

        return nullptr;
    }

    Node<E>* _deleteSubtree(Node<E>* root, int &key)
    {
        if (root == nullptr) return root;

        if (key < root->key)
            root->left = _deleteSubtree(root->left, key);

        else if (key > root->key)
            root->right = _deleteSubtree(root->right, key);

        else
        {
            auto it = TreeIterator<E>::preorder(root);
            int i = 0;
            while (it.hasNext())
            {
                delete it.nextRaw();
                i++;
            }

            m_size -= i;

            root = nullptr;
        }

        return root;
    }

public:
    Tree()
    {
        m_size = 0;
        root = nullptr;
    }

    Tree(std::vector<std::pair<int, E>> &items)
    {
        std::sort(items.begin(), items.end(), [](std::pair<int, E> a, std::pair<int, E> b) {return a.first > b.first; });

        std::vector<Node<E>*> nodelist;
        for (auto p : items)
        {
            nodelist.push_back(new Node<E>(p.first, p.second));
        }

        m_size = nodelist.size();
        root = _list_to_balanced_tree(nodelist, 0, static_cast<int>(nodelist.size()) - 1);
    }

    void add(int key, E item)
    {
        auto node = new Node<E>(key, item);

        if (empty())
        {
            root = node;
            m_size++;
        }
        else
            _add(root, node);
    }

    void printDebug()
    {
        _print();
    }

    int size() { return m_size; }

    void balance()
    {
        std::vector<Node<E>*> nodelist;
        _tree_to_list(nodelist, root);

        root = _list_to_balanced_tree(nodelist, 0, static_cast<int>(nodelist.size()) - 1);
    }

    void remove(int key)
    {
        root = _deleteNode(root, key);
    }

    Node<E>* getRoot()
    {
        return root;
    }

    Node<E>* get(int key)
    {
        return _get(root, key);
    }

    TreeIterator<E> iterator()
    {
        return TreeIterator<E>::inorder(root);
    }

    bool empty()
    {
        return root == nullptr;
    }

    void removeSubtree(int key)
    {
        root = _deleteSubtree(root, key);
    }
};

#endif // TREE_H
