#include "dialog.h"
#include "ui_dialog.h"

#include <QStringList>

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    t.add(5, "a");
    t.add(2, "b");
    t.add(7, "c");
    t.add(3, "d");
    t.add(1, "e");
    t.add(4, "g");
    t.add(8, "f");
    t.add(9, "h");
    t.add(6, "i");
    t.add(11, "j");
    t.add(10, "k");
    t.add(12, "l");
    t.add(13, "m");
    t.add(14, "n");

//    t.printDebug();
//    qDebug() << "------------------------";
//    t.balance();
//    t.printDebug();

//    auto it = t.iterator();
//    while (it.hasNext())
//    {
//        qDebug() << it.next();
//    }

//    qDebug() << t.get(0);

    auto it = TreeIterator<QString>::postorder(t.getRoot());

    while (it.hasNext())
    {
        qDebug() << it.next();
    }

    rebuild();

    connect(ui->balanceButton, &QPushButton::clicked, this, [=] { t.balance(); rebuild(); });
    connect(ui->addButton, &QPushButton::clicked, this, &Dialog::addPressed);
    connect(ui->removeButton, &QPushButton::clicked, this, &Dialog::removePressed);
    connect(ui->subtreeRemoveButton, &QPushButton::clicked, this, &Dialog::removeSubtreePressed);
    connect(ui->getButton, &QPushButton::clicked, this, &Dialog::getPressed);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::buildTree(Node<QString>* from, QTreeWidgetItem *to)
{
    if (from->left != nullptr) buildTree(from->left, new QTreeWidgetItem(to));
    to->setText(0, QString("%1:%2").arg(from->data, QString::number(from->key)));
    if (from->right != nullptr) buildTree(from->right, new QTreeWidgetItem(to));
    to->setExpanded(true);
}

void Dialog::rebuild()
{
    ui->keySpinBox->setValue(t.size() + 1);

    ui->treeWidget->clear();

    if (t.empty()) return;

    auto root = t.getRoot();

    auto top = new QTreeWidgetItem(ui->treeWidget);

    buildTree(root, top);
}

void Dialog::addPressed()
{
    t.add(ui->keySpinBox->value(), ui->valueEditor->text());
    rebuild();
}

void Dialog::removePressed()
{
    auto item = ui->treeWidget->currentItem();
    if (item != nullptr)
    {
        QStringList l = item->text(0).split(':');
        t.remove(l.at(1).toInt());
        rebuild();
    }
}

void Dialog::removeSubtreePressed()
{
    auto item = ui->treeWidget->currentItem();
    if (item != nullptr)
    {
        QStringList l = item->text(0).split(':');
        t.removeSubtree(l.at(1).toInt());
        rebuild();
    }
}

void Dialog::getPressed()
{
    auto node = t.get(ui->keySpinBox->value());
    if (node != nullptr) ui->valueEditor->setText(node->data);
    else ui->valueEditor->setText("No such key!");
}
