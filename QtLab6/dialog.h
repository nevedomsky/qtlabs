#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QDebug>
#include <QTreeWidgetItem>

#include "tree.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    Ui::Dialog *ui;

    Tree<QString> t;

    void buildTree(Node<QString> *from, QTreeWidgetItem *to);
    void rebuild();
    void addPressed();
    void removePressed();
    void removeSubtreePressed();
    void getPressed();
};

#endif // DIALOG_H
