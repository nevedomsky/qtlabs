#include "dialog.h"
#include "ui_dialog.h"
#include <QDebug>


Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    for (int i = 0; i < 30; i++)
    {
        numbers.push_back(QRandomGenerator::global()->generate() % 1000);
    }

    refresh();

    connect(ui->processButton, &QPushButton::clicked, this, &Dialog::process);
    connect(ui->addButton, &QPushButton::clicked, this, [=]
    {
       numbers.push_back(ui->spinBox->value());
       refresh();
    });

    connect(ui->removeButton, &QPushButton::clicked, this, [=]
    {
       int index = ui->listWidget->currentRow();
       if (index < 0) return;

       numbers.removeAt(index);
       refresh();
    });
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::refresh()
{
    ui->listWidget->clear();
    auto it = numbers.iterator();

    int i = 0;
    int maxi = -1;
    int mini = -1;
    int max = -1;
    int min = 2000000000;

    while (it.hasNext())
    {
        int x = it.next();
        ui->listWidget->addItem(QString::number(x));
        ui->listWidget->item(i)->setTextAlignment(Qt::AlignHCenter);

        if (x > max)
        {
            max = x;
            maxi = i;
        }

        if (x < min)
        {
            min = x;
            mini = i;
        }
        i++;
    }

    ui->listWidget->item(mini)->setBackgroundColor(QColor(Qt::yellow));
    ui->listWidget->item(maxi)->setBackgroundColor(QColor(Qt::green));
}

void Dialog::process()
{
    numbers.removeBetween();

    refresh();
}
