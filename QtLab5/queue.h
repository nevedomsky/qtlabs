#ifndef QUEUE_H
#define QUEUE_H

#include "list.h"

template <typename E>
class Queue : private List
{
public:
    Queue() : List()
    {}

    void push(E item)
    {
        List::push_back(item);
    }

    E pop()
    {
        E temp = List::front();
        List::pop_front();
        return temp;
    }

    E peek()
    {
        return List::front();
    }
}

#endif // QUEUE_H
