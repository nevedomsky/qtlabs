#ifndef LIST_H
#define LIST_H

template<typename E>
class Node
{
public:
    E data;
    Node* next;
    Node* prev;

    Node(Node* prev, E item, Node* next) : data(item) {
        this->next = next;
        this->prev = prev;
    }

    Node(E item) : Node(nullptr, item, nullptr) {}
};

template <typename E>
class Iterator
{
    Node<E>* current;
public:
    Iterator(Node<E>* from) : current(from) {}

    bool hasNext()
    {
        return current != nullptr;
    }

    E next()
    {
        auto item = current->data;
        current = current->next;
        return item;
    }
};

template <typename E>
class List
{
    Node<E> *tail;
    Node<E> *head;

    int m_size;

public:
    List()
    {
        m_size = 0;
        tail = head = nullptr;
    }

    int size()
    {
        return m_size;
    }

    void push_back(E item)
    {
        auto node = new Node<E>(item);
        m_size++;

        if (tail == nullptr)
        {
            tail = head = node;
        }
        else
        {
            tail->next = node;
            node->prev = tail;
            tail = node;
        }
    }

    void push_front(E item)
    {
        auto node = new Node<E>(item);
        m_size++;

        if (head == nullptr)
        {
            head = tail = node;
        }
        else
        {
            head->prev = node;
            node->next = head;
            head = node;
        }
    }

    E back()
    {
        return tail->data;
    }

    E front()
    {
        return head->data;
    }

    void pop_back()
    {
        m_size--;
        auto node = tail;
        tail = tail->prev;
        delete node;
    }

    void pop_front()
    {
        m_size--;
        auto node = head;
        head = head->next;
        delete node;
    }

    bool empty()
    {
        return m_size == 0;
    }

    E at(int index)
    {
        auto node = head;
        for (int i = 0; i < index; i++)
        {
            node = node->next;
        }

        return node->data;
    }

    Iterator<E> iterator()
    {
        return Iterator<E>(head);
    }

    void removeAt(int index)
    {
        auto node = head;
        for (int i = 0; i < index; i++)
        {
            node = node->next;
        }

        if (node->prev != nullptr)
            node->prev->next = node->next;
        else head = node->next;

        if (node->next != nullptr)
            node->next->prev = node->prev;
        else tail= tail->prev;

        delete node;
    }

    void insertAt(int index, E item)
    {
        auto node = head;
        for (int i = 0; i < index - 1; i++)
        {
            node = node->next;
        }

        auto newnode = new Node<E>{item};


    }
};

#endif // LIST_H
