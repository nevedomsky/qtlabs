#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QRandomGenerator>

#include "list.h"
#include "minmaxlist.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    Ui::Dialog *ui;

    MinMaxList<int> numbers;
    void process();
    void refresh();
};

#endif // DIALOG_H
