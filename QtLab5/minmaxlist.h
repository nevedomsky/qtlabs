#ifndef MINMAXLIST_H
#define MINMAXLIST_H

#include "list.h"

template <typename E>
class MinMaxList : public List<E>
{
public:
    MinMaxList() : List<E>()
    {}

    void removeBetween()
    {
        auto it = List::iterator();

        int i = 0;
        int maxi = -1;
        int mini = -1;
        int max = -1;
        int min = 2000000000;

        while (it.hasNext())
        {
            int x = it.next();

            if (x > max)
            {
                max = x;
                maxi = i;
            }

            if (x < min)
            {
                min = x;
                mini = i;
            }
            i++;
        }

        if (mini > maxi)
        {
            int temp = maxi;
            maxi = mini;
            mini = temp;
        }

        for (int i = 0; i < maxi - mini - 1; i++) List::removeAt(mini+1);
    }
};

#endif // MINMAXLIST_H
