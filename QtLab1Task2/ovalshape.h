#ifndef OVALSHAPE_H
#define OVALSHAPE_H
#include <QObject>
#include "shape.h"

class OvalShape : public Shape
{
    Q_OBJECT

private:
    double m_diff;
    QPointF m_pos;

public:
    OvalShape(QPointF pos, double rad_a, double rad_b);

    double getArea();
    double getPerimeter();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR);
    QRectF boundingRect() const;
    QPainterPath shape() const;

};

#endif // OVALSHAPE_H
