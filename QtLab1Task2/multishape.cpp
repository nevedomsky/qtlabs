#include "multishape.h"
#define M_PI 3.14159265358979323846264338327950288

MultiShape::MultiShape(QPointF pos, double radius, int sides = 4) : m_sides(sides), Shape(pos)
{
    m_size = radius;
}

double MultiShape::getArea()
{
    return  m_sides * pow(m_size, 2) / 2 * sin(2. * M_PI / m_sides);
}

double MultiShape::getPerimeter()
{
    return 2 * m_sides * m_size * sin(M_PI / m_sides);
}

void MultiShape::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QPolygonF polygon;
    double step = M_PI * 2 / m_sides;
    double startangle = M_PI / 4;

    for (int i = 0; i < m_sides; i++)
    {
        polygon.append(QPointF(m_size * cos(i * step + startangle), m_size * sin(i * step + startangle)));
    }

    painter->setPen(pen());
    painter->setBrush(brush());
    painter->drawPolygon(polygon);

    if (showCenterPoint)
    {
        painter->setPen(QPen(QBrush(Qt::black), 1));
        painter->setBrush(Qt::black);
        painter->drawEllipse(QPointF(0, 0), 3, 3);
    }
}

QRectF MultiShape::boundingRect() const
{
    qreal penWidth = pen().widthF();

    return QRectF(
                -m_size - penWidth / 2,
                -m_size - penWidth / 2,
                m_size * 2 + penWidth,
                m_size * 2 + penWidth
                );
}

QPainterPath MultiShape::shape() const
{
    QPolygonF polygon;
    double step = M_PI * 2 / m_sides;
    double startangle = M_PI / 4;

    for (int i = 0; i < m_sides; i++)
    {
        polygon.append(QPointF(m_size * cos(i * step + startangle), m_size * sin(i * step + startangle)));
    }
    QPainterPath path;
    //path.addEllipse(boundingRect());
    path.addPolygon(polygon);
    return path;
}
