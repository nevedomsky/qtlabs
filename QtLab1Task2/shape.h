#ifndef SHAPE_H
#define SHAPE_H
#include <QGraphicsItem>
#include <QGraphicsObject>
#include <QGraphicsSceneHoverEvent>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QCursor>

class Shape : public QObject, public QAbstractGraphicsShapeItem
{
    Q_OBJECT

private:
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

public:
    Shape(QPointF pos);

    virtual double getArea() = 0;
    virtual double getPerimeter() = 0;

    void setSize(double size) { prepareGeometryChange(); m_size = size; update(); }
    double size() { return m_size; }

protected:
    double m_size;
    bool showCenterPoint = false;
};

#endif // SHAPE_H
