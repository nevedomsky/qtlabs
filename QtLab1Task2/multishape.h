#ifndef MULTISHAPE_H
#define MULTISHAPE_H
#include <QObject>
#include "shape.h"

class MultiShape :public Shape
{
    Q_OBJECT
    Q_PROPERTY(int sides READ sides WRITE setSides)

private:
    int m_sides;
    double m_angle;

public:
    MultiShape(QPointF pos, double radius, int sides);

    void setSides(int sides) { prepareGeometryChange(); m_sides = sides; update(); }
    int sides() { return m_sides; }

    double getArea();
    double getPerimeter();

    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget = Q_NULLPTR);
    QRectF boundingRect() const;
    QPainterPath shape() const;
};

#endif // MULTISHAPE_H
