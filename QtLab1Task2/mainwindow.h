#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPropertyAnimation>
#include <QGraphicsScene>
#include <QDebug>
#include "multishape.h"
#include "ovalshape.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    static int getZ() {return ++z;}
    int getItemsAdded() { return ++itemsAdded; }

private:
    QGraphicsScene *scene;
    static int z;
    int itemsAdded = 0;
    Ui::MainWindow *ui;

    QList<Shape*> shapes;
    void AddItem();

};

#endif // MAINWINDOW_H
