#include "mainwindow.h"
#include "ui_mainwindow.h"

int MainWindow::z = 0;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Shapes"));
    setWindowState(Qt::WindowMaximized);

    scene = new QGraphicsScene(this);

    ui->graphicsView->setRenderHint(QPainter::HighQualityAntialiasing);
    ui->graphicsView->setCacheMode(QGraphicsView::CacheBackground);
    ui->graphicsView->setScene(scene);
    scene->setSceneRect(ui->graphicsView->rect());

    ui->shapeSelector->addItem("Oval", 1);
    ui->shapeSelector->addItem("MultiShape", 2);
    ui->shapeSelector->addItem("Triangle", 3);
    ui->shapeSelector->addItem("Square", 4);
    ui->shapeSelector->addItem("Circle", 5);

    connect(ui->addButton, &QPushButton::released, this, &MainWindow::AddItem);

//    auto anim = new QPropertyAnimation(shape, "radius");
//    anim->setStartValue(10.);
//    anim->setEndValue(300.);
//    anim->setDuration(3000);
//    auto anim = new QPropertyAnimation(shape, "sides");
//    anim->setStartValue(3);
//    anim->setEndValue(50);
//    anim->setDuration(25000);
//    anim->setEasingCurve(QEasingCurve::Linear);
//    anim->setLoopCount(1000);
//    anim->start();

    connect(
        ui->verticalSlider, &QSlider::valueChanged,
        this, [=] (const int &value) {
            int index = ui->shapesList->currentIndex().row();
            if (index < 0) return;
            shapes.at(index)->setSize(value);
        }
    );

    connect(
        ui->dial, &QDial::valueChanged,
        this, [=]( const qreal &newValue ) {
            int index = ui->shapesList->currentIndex().row();
            if (index >= 0) shapes.at(index)->setRotation(newValue);
        }
    );

    connect(
        ui->shapesList, &QListWidget::currentItemChanged,
        this, [=] {
            int index = ui->shapesList->currentIndex().row();
            if (index < 0) return;
            ui->dial->setValue(shapes.at(index)->rotation());
            ui->verticalSlider->setValue(shapes.at(index)->size());
        }
    );

    connect(
        ui->removeButton, &QPushButton::released,
        this, [=] {
            int index = ui->shapesList->currentIndex().row();
            if (index < 0) return;

            auto item = shapes.at(index);
            scene->removeItem(item);
            shapes.removeAt(index);
            delete ui->shapesList->takeItem(index);
        }
    );
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::AddItem()
{
    Shape* shape;

    switch (ui->shapeSelector->currentIndex())
    {
        case 0:
            shape = new OvalShape(QPointF(0, 0), 100, 200);
            break;
        case 1:
            shape = new MultiShape(QPointF(0, 0), 200., 7);
            break;
        case 2:
            shape = new MultiShape(QPointF(0, 0), 200., 3);
            break;
        case 3:
            shape = new MultiShape(QPointF(0, 0), 200., 4);
            break;
        case 4:
            shape = new OvalShape(QPointF(0, 0), 150, 150);
            break;
    }

    shape->setPen(QPen(QBrush(Qt::red), 5.));
    shape->setBrush(QBrush(Qt::yellow));

    shapes << shape;
    ui->shapesList->addItem(QString("%1 %2").arg(QString::number(shapes.size()), shape->metaObject()->className()));
    ui->shapesList->setCurrentRow(shapes.size() - 1);
    scene->addItem(shape);
}
