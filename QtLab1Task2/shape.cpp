#include "shape.h"
#include "mainwindow.h"

Shape::Shape(QPointF pos)
{
    //setCacheMode(ItemCoordinateCache);
    setFlag(ItemIsMovable);
    setAcceptHoverEvents(true);
    setPos(pos);
}

void Shape::hoverMoveEvent(QGraphicsSceneHoverEvent* event)
{
    double area = getArea();
    double per = getPerimeter();

    QString info = QString("Perimeter: %1\nArea: %2")
            .arg(QString::number(per), QString::number(area));

    if(showCenterPoint)
    {
        QString point = QString("\nX: %1 Y: %2").arg(QString::number(scenePos().x()), QString::number(scenePos().y()));
        info.append(point);
    }

    setToolTip(info);
    QGraphicsItem::hoverMoveEvent(event);
    Q_UNUSED(event);
}

void Shape::hoverEnterEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(QCursor(Qt::OpenHandCursor));
    QGraphicsItem::hoverEnterEvent(event);
}

void Shape::hoverLeaveEvent(QGraphicsSceneHoverEvent *event)
{
    setCursor(QCursor(Qt::ArrowCursor));
    QGraphicsItem::hoverLeaveEvent(event);
}

void Shape::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() == Qt::RightButton)
    {
        showCenterPoint = !showCenterPoint;
        update();
    }
    else
    {
        setZValue(MainWindow::getZ());
        setCursor(QCursor(Qt::ClosedHandCursor));
    }
    QGraphicsItem::mousePressEvent(event);
}

void Shape::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    setCursor(QCursor(Qt::OpenHandCursor));
    QGraphicsItem::mouseReleaseEvent(event);
}
