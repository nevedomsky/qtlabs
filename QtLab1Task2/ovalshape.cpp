#include "ovalshape.h"
#define M_PI 3.14159265358979323846264338327950288
#include <QStyleOptionGraphicsItem>

OvalShape::OvalShape(QPointF pos, double rad_a, double rad_b) : Shape(pos)
{
    m_size = rad_a;
    m_diff = rad_b - rad_a;
}

double OvalShape::getArea()
{
    return M_PI * m_size * (m_size + m_diff);
}

double OvalShape::getPerimeter()
{
    return 4 * (getArea() + pow(m_diff, 2)) / (m_size * 2 + m_diff);
}

void OvalShape::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->setPen(pen());
    painter->setBrush(brush());
    painter->drawEllipse(QPointF(0, 0), m_size, m_size + m_diff);

    if (showCenterPoint)
    {
        painter->setPen(QPen(QBrush(Qt::black), 1));
        painter->setBrush(Qt::black);
        painter->drawEllipse(QPointF(0, 0), 3, 3);
    }
}

QRectF OvalShape::boundingRect() const
{
    qreal penWidth = pen().widthF();

    return QRectF(
                -m_size - penWidth / 2,
                -(m_size + m_diff) - penWidth / 2,
                m_size * 2 + penWidth,
                (m_size + m_diff) * 2 + penWidth
                );
}

QPainterPath OvalShape::shape() const
{
    QPainterPath path;
    path.addEllipse(boundingRect());
    return path;
}

