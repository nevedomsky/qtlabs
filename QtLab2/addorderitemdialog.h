#ifndef ADDORDERITEMDIALOG_H
#define ADDORDERITEMDIALOG_H

#include "dbmanager.h"

#include <QDialog>
#include <QList>
#include "data.h"

namespace Ui {
class AddOrderItemDialog;
}

class AddOrderItemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddOrderItemDialog(DbManager* manager, int id, QWidget *parent = 0);
    ~AddOrderItemDialog();

private:
    DbManager* m_manager;
    QList<Item> items;
    int order_id;

    Ui::AddOrderItemDialog *ui;

    void addItemClicked();
    void reloadItems();
    void reloadTotal();
    void acceptClicked();
};

#endif // ADDORDERITEMDIALOG_H
