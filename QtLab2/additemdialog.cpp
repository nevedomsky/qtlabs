#include "additemdialog.h"
#include "ui_additemdialog.h"

#include <QMessageBox>

AddItemDialog::AddItemDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddItemDialog)
{
    ui->setupUi(this);

    connect(ui->acceptButton, &QPushButton::clicked, this, &AddItemDialog::confirm);
    connect(ui->declineButton, &QPushButton::clicked, this, &AddItemDialog::reject);
}

AddItemDialog::~AddItemDialog()
{
    delete ui;
}

QString AddItemDialog::getName()
{
    return ui->itemNameEditor->text();
}

double AddItemDialog::getPrice()
{
    return ui->itemPriceEditor->value();
}

void AddItemDialog::confirm()
{
    if (validate()) accept();
    else
    {
        QMessageBox::warning(this, tr("Incorrect format"), tr("Some field have incorrect format."));
    }
}

bool AddItemDialog::validate()
{
    bool first = !ui->itemNameEditor->text().isEmpty();
    bool second = ui->itemPriceEditor->value() > 0;

    return first && second;
}
