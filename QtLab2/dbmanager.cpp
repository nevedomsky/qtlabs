#include "dbmanager.h"

#include <QVariant>
#include <QDebug>

DbManager::DbManager(const QString& path)
{
   m_db = QSqlDatabase::addDatabase("QSQLITE");
   m_db.setDatabaseName(path);

   if (!m_db.open())
   {
      qDebug() << "Error: connection with database fail";
   }
   else
   {
      qDebug() << "Database: connection ok";
   }
}

void DbManager::ListAll()
{
//    QSqlQuery query("SELECT client,name,amount,price FROM item_order "
//                    "JOIN items ON item_id == items.id "
//                    "JOIN orders ON order_id == orders.id;");

    QSqlQuery clientsQuery("SELECT client,id,date FROM orders;");

    QVector<int> orders;
    while (clientsQuery.next())
    {
        qDebug() << clientsQuery.value(0).toString() << "on" << clientsQuery.value(2).toString() << "wants to buy:";
        orders.push_back(clientsQuery.value(1).toInt());
    }

    for (auto order : orders)
    {
        QSqlQuery itemsQuery;
        itemsQuery.prepare("SELECT item_id,amount FROM item_order WHERE order_id = ?");
        itemsQuery.addBindValue(order);
        itemsQuery.exec();

        QVector<QPair<int, int>> items;
        while (itemsQuery.next())
        {
            items.push_back(QPair<int, int>(itemsQuery.value(0).toInt(), itemsQuery.value(1).toInt()));
        }

        int sum = 0;

        for (auto item : items)
        {
            QSqlQuery query;
            query.prepare("SELECT name,price FROM items WHERE id = ?");
            query.addBindValue(item.first);
            query.exec();

            while (query.next())
            {
                sum += item.second * query.value(1).toInt();
                QString price = QString("%1x%2$ of %3")
                        .arg(
                            QString::number(item.second),
                            QString::number(query.value(1).toInt()),
                            query.value(0).toString()
                        );
                qDebug() << price;
                //qDebug() << item.second << "x" << query.value(1).toInt() << " of " << query.value(0).toString();
            }
        }

        qDebug() << "Total of" << sum << "\n";
    }
//    qDebug() << "All Entries: \n";
//    while (query.next())
//    {
//        for (int i = 0; i < query.record().count(); i++)
//            qDebug() << query.value(i).toString();

//        qDebug() << "\n";
    //    }
}

QList<Order> DbManager::getAllOrders()
{
    QList<Order> orders;
    QSqlQuery clientsQuery("SELECT client,id,date FROM orders;");

    while (clientsQuery.next())
    {
        QString client(clientsQuery.value(0).toString());
        int id(clientsQuery.value(1).toInt());
        QString date(clientsQuery.value(2).toString());

        Order order(id, client, date);

        orders.push_back(order);
    }

    for (int i = 0; i < orders.size(); i++)
    {
        QSqlQuery itemsQuery;
        itemsQuery.prepare("SELECT item_id,amount FROM item_order WHERE order_id = ?");
        itemsQuery.addBindValue(orders[i].getOrderId());
        itemsQuery.exec();

        QList<QPair<int, int>> items;
        while (itemsQuery.next())
        {
            items.push_back(QPair<int, int>(itemsQuery.value(0).toInt(), itemsQuery.value(1).toInt()));
        }

        for (auto item : items)
        {
            QSqlQuery query;
            query.prepare("SELECT name,price FROM items WHERE id = ?");
            query.addBindValue(item.first);
            query.exec();

            if (query.next())
            {
                QString name{query.value(0).toString()};
                double price{query.value(1).toDouble()};

                Item newitem{item.first, name, price};

                orders[i].addItem(newitem, item.second);
            }
        }
    }

    return orders;
}

QList<Item> DbManager::getAllItems()
{
    QList<Item> items;
    QSqlQuery query("SELECT name,price,id FROM items;");

    while (query.next())
    {
        QString name{query.value(0).toString()};
        double price{query.value(1).toDouble()};
        int id{query.value(2).toInt()};

        items.push_back(Item(id, name, price));
    }

    return items;
}

void DbManager::addNewItem(QString name, double price)
{
    QSqlQuery query;
    query.prepare("INSERT INTO items (name,price) VALUES (?,?);");
    query.addBindValue(name);
    query.addBindValue(price);
    query.exec();
}

void DbManager::addItemToOrder(int order_id, int item_id, int amount)
{
    QSqlQuery query;
    query.prepare("INSERT INTO item_order (order_id,item_id,amount) VALUES (?,?,?);");
    query.addBindValue(order_id);
    query.addBindValue(item_id);
    query.addBindValue(amount);
    query.exec();
}

void DbManager::removeItemFromOrder(int order_id, int item_id)
{
    QSqlQuery query;
    query.prepare("DELETE FROM item_order WHERE order_id = ? AND item_id = ?;");
    query.addBindValue(order_id);
    query.addBindValue(item_id);
    query.exec();
}

void DbManager::addOrder(QString client)
{
    QSqlQuery query;
    query.prepare("INSERT INTO orders (client,date) VALUES (?,?);");
    query.addBindValue(client);
    query.addBindValue(QDateTime::currentDateTime().toString(Order::format));
    query.exec();
}

void DbManager::removeOrder(int id)
{
    QSqlQuery queryItem;
    queryItem.prepare("DELETE FROM item_order WHERE order_id = ?;");
    queryItem.addBindValue(id);
    queryItem.exec();

    QSqlQuery queryOrder;
    queryOrder.prepare("DELETE FROM orders WHERE id = ?;");
    queryOrder.addBindValue(id);
    queryOrder.exec();
}

void DbManager::updateAmount(int item_id, int order_id, int amount)
{
    QSqlQuery queryItem;
    queryItem.prepare("UPDATE item_order SET amount = ? WHERE item_id = ? AND order_id = ?;");
    queryItem.addBindValue(amount);
    queryItem.addBindValue(item_id);
    queryItem.addBindValue(order_id);
    queryItem.exec();
}

QList<QPair<Item, int>> DbManager::getItemsForOrder(int id)
{
    QList<QPair<Item, int>> litems;
    QSqlQuery itemsQuery;
    itemsQuery.prepare("SELECT item_id,amount FROM item_order WHERE order_id = ?");
    itemsQuery.addBindValue(id);
    itemsQuery.exec();

    QList<QPair<int, int>> items;
    while (itemsQuery.next())
    {
        items.push_back(QPair<int, int>(itemsQuery.value(0).toInt(), itemsQuery.value(1).toInt()));
    }

    for (auto item : items)
    {
        QSqlQuery query;
        query.prepare("SELECT name,price FROM items WHERE id = ?");
        query.addBindValue(item.first);
        query.exec();

        if (query.next())
        {
            QString name{query.value(0).toString()};
            double price{query.value(1).toDouble()};

            Item newitem{item.first, name, price};

            litems.push_back(QPair<Item, int>(newitem, item.second));
        }
    }

    return litems;
}
