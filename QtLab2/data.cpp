#include "data.h"

const QString Order::format = "yyyy-MM-dd hh:mm:ss";

Order::Order(int id, QString client, QString date) : m_id(id), m_client_name(client)
{
    m_datetime = QDateTime::fromString(date, format);
    m_items = QList<QPair<Item, int>>{};
}

void Order::addItem(Item item, int amount)
{
    m_items.push_back(QPair<Item, int>(item, amount));
}

QString Order::getClientName()
{
    return m_client_name;
}

QList<QPair<Item, int>> Order::getAllItems() const
{
    return m_items;
}

QDateTime Order::getOrderCreationTime()
{
    return m_datetime;
}

int Order::getOrderId() const
{
    return m_id;
}

double Order::getFullPrice()
{
    double sum = 0.;

    for (auto item : m_items)
    {
        sum += (item.first.getPrice() * item.second);
    }

    return sum;
}

QString Order::toString()
{
    QString row = QString("%1 | %2 |%3$").arg(
                getClientName().leftJustified(23, ' '),
                getOrderCreationTime().toString(format),
                QString::number(getFullPrice(), 'f', 2).rightJustified(10)
    );
    return row;
}

Item::Item(int id, QString name, double price) : m_id(id), m_name(name), m_price(price)
{
}

int Item::getId() const
{
    return m_id;
}

double Item::getPrice() const
{
    return m_price;
}

QString Item::getItemName()
{
    return m_name;
}

QString Item::toString(int count)
{
    QString row = QString(" %1  |%2 x%3").arg(
                getItemName().leftJustified(38, ' '),
                QString::number(count).rightJustified(4),
                QString::number(getPrice(), 'f', 2).append('$').rightJustified(10)
    );

    return row;
}
