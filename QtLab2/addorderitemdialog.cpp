#include "addorderitemdialog.h"
#include "ui_addorderitemdialog.h"

#include "additemdialog.h"

#include <QMessageBox>

AddOrderItemDialog::AddOrderItemDialog(DbManager* manager, int id, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddOrderItemDialog),
    m_manager(manager),
    order_id(id)
{
    ui->setupUi(this);

    reloadItems();
    reloadTotal();

    connect(ui->addItemButton, &QPushButton::clicked, this, &AddOrderItemDialog::addItemClicked);
    connect(ui->itemSelector, QOverload<int>::of(&QComboBox::currentIndexChanged), [=](int){ reloadTotal();});
    connect(ui->amountSelector, QOverload<int>::of(&QSpinBox::valueChanged), [=](int){ reloadTotal(); });

    connect(ui->acceptButton, &QPushButton::clicked, this, &AddOrderItemDialog::acceptClicked);
    connect(ui->declineButton, &QPushButton::clicked, this, &AddOrderItemDialog::reject);
}

AddOrderItemDialog::~AddOrderItemDialog()
{
    delete ui;
}

void AddOrderItemDialog::acceptClicked()
{
    int index = ui->itemSelector->currentIndex();
    if (index < 0)
    {
        QMessageBox::warning(this, tr("Error!"), tr("No items selected!"));
        return;
    }
    m_manager->addItemToOrder(order_id, items.at(index).getId(), ui->amountSelector->value());
    accept();
}

void AddOrderItemDialog::addItemClicked()
{
    AddItemDialog d;

    if (d.exec())
    {
        m_manager->addNewItem(d.getName(), d.getPrice());

        reloadItems();
    }
}

void AddOrderItemDialog::reloadTotal()
{
    int index = ui->itemSelector->currentIndex();
    if (index < 0) return;

    int sum = items.at(index).getPrice() * ui->amountSelector->value();
    ui->itemTotalLabel->setText(QString("Total sum: %1$").arg(QString::number(sum)));
}

void AddOrderItemDialog::reloadItems()
{
    items = m_manager->getAllItems();

    ui->itemSelector->clear();

    for (auto item : items)
    {
        ui->itemSelector->addItem(item.getItemName());
    }

    ui->itemSelector->setCurrentIndex(0);
}
