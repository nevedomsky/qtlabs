#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "data.h"
#include "dbmanager.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

    void refreshItemList(bool triggered = false);
    void refreshOrderList();
private:
    QList<Order> orders;
    DbManager *manager;

    Ui::Dialog *ui;

    void printButtonClicked();

    void addItemButtonClicked();
    void removeItemButtonClicked();

    void addOrderButtonClicked();
    void removeOrderButtonClicked();

    void increaseButtonClicked(int i);
};

#endif // DIALOG_H
