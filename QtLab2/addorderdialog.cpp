#include "addorderdialog.h"
#include "ui_addorderdialog.h"

#include <QMessageBox>

AddOrderDialog::AddOrderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddOrderDialog)
{
    ui->setupUi(this);

    connect(ui->acceptButton, &QPushButton::clicked, this, &AddOrderDialog::confirm);
    connect(ui->declineButton, &QPushButton::clicked, this, &AddOrderDialog::reject);
}

AddOrderDialog::~AddOrderDialog()
{
    delete ui;
}

QString AddOrderDialog::getName()
{
    return ui->clientNameEditor->text();
}

void AddOrderDialog::confirm()
{
    if (validate()) accept();
    else
    {
        QMessageBox::warning(this, tr("Incorrect format"), tr("Name must be non-empty!"));
    }
}

bool AddOrderDialog::validate()
{
    bool first = !ui->clientNameEditor->text().isEmpty();

    return first;
}
