#ifndef DATA_H
#define DATA_H

#include <QDateTime>
#include <QList>
#include <QPair>
#include <QString>

class Item
{
private:
    int m_id;
    QString m_name;
    double m_price;
public:
    Item(int id, QString name, double price);

    int getId() const;
    double getPrice() const;
    QString getItemName();

    QString toString(int count);
};

class Order
{

private:
    QList<QPair<Item, int>> m_items;
    QString m_client_name;
    int m_id;
    QDateTime m_datetime;

public:
    static const QString format;

    Order(int id, QString client, QString date);

    void addItem(Item item, int amount);

    QString getClientName();
    QList<QPair<Item, int>> getAllItems() const;
    QDateTime getOrderCreationTime();
    int getOrderId() const;

    double getFullPrice();
    QString toString();
};

#endif // DATA_H
