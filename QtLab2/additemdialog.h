#ifndef ADDITEMDIALOG_H
#define ADDITEMDIALOG_H

#include <QDialog>

namespace Ui {
class AddItemDialog;
}

class AddItemDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddItemDialog(QWidget *parent = 0);
    ~AddItemDialog();

    QString getName();
    double getPrice();

private:
    Ui::AddItemDialog *ui;
    bool validate();
    void confirm();
};

#endif // ADDITEMDIALOG_H
