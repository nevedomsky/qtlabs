#include "dialog.h"
#include "ui_dialog.h"

#ifndef QT_NO_PRINTER
#include <QPrinter>
#include <QPrintDialog>
#include <QPainter>
#include <QFont>
#endif
#include "addorderitemdialog.h"
#include "addorderdialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);
    //setWindowState(Qt::WindowMaximized);

    QString dbPath = QStandardPaths::writableLocation(QStandardPaths::DesktopLocation).append("/orders.db");

    manager = new DbManager(dbPath);

    refreshOrderList();
    refreshItemList();

    connect(ui->orderList, &QListWidget::currentRowChanged, this, [=] { refreshItemList(true); });

    connect(ui->addItemButton, &QPushButton::clicked, this, &Dialog::addItemButtonClicked);
    connect(ui->removeItemButton, &QPushButton::clicked, this, &Dialog::removeItemButtonClicked);

    connect(ui->addOrderButton, &QPushButton::clicked, this, &Dialog::addOrderButtonClicked);
    connect(ui->removeOrderButton, &QPushButton::clicked, this, &Dialog::removeOrderButtonClicked);

    connect(ui->printButton, &QPushButton::clicked, this, &Dialog::printButtonClicked);
    connect(ui->increaseCountButton, &QToolButton::clicked, this, [=] { increaseButtonClicked(1); });
    connect(ui->decreaseCountButton, &QToolButton::clicked, this, [=] { increaseButtonClicked(-1); });
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::addItemButtonClicked()
{
    int index = ui->orderList->currentRow();
    if (index < 0) return;

    int id = orders.at(index).getOrderId();
    AddOrderItemDialog d(manager, id, this);
    if (d.exec())
    {
        refreshItemList();
    }
}

void Dialog::removeItemButtonClicked()
{
    int orderindex = ui->orderList->currentRow();
    if (orderindex < 0) return;

    int order_id = orders.at(orderindex).getOrderId();

    int itemindex = ui->itemList->currentRow();
    if (itemindex < 0) return;

    int item_id = orders.at(orderindex).getAllItems().at(itemindex).first.getId();

    manager->removeItemFromOrder(order_id, item_id);

    refreshItemList();
}

void Dialog::addOrderButtonClicked()
{
    AddOrderDialog d(this);
    if (d.exec())
    {
        manager->addOrder(d.getName());
        refreshOrderList();
    }
}

void Dialog::removeOrderButtonClicked()
{
    int orderindex = ui->orderList->currentRow();
    if (orderindex < 0) return;

    manager->removeOrder(orders[orderindex].getOrderId());

    refreshOrderList();
}

void Dialog::increaseButtonClicked(int i = 1)
{
    int index = ui->orderList->currentRow();
    if (index < 0) return;

    auto order = orders.at(index);

    index = ui->itemList->currentRow();
    if (index < 0) return;

    auto item = order.getAllItems().at(index);
    int amount = item.second;

    if (i < 0 && amount <= 1) return;

    amount += i;

    manager->updateAmount(item.first.getId(), order.getOrderId(), amount);

    refreshItemList();

    ui->itemList->setCurrentRow(index);
}

void Dialog::refreshItemList(bool triggered)
{
    ui->itemList->clear();

    int index = ui->orderList->currentRow();
    if (index < 0) return;

    for (auto item : manager->getItemsForOrder(orders.at(index).getOrderId()))
    {
        ui->itemList->addItem(item.first.toString(item.second));
    }

    if (!triggered)
    {
        refreshOrderList();
        ui->orderList->setCurrentRow(index);
    }
}

void Dialog::refreshOrderList()
{
    ui->itemList->clear();
    ui->orderList->clear();

    orders = manager->getAllOrders();

    for (auto order : orders)
    {
        ui->orderList->addItem(order.toString());
    }
}

void Dialog::printButtonClicked()
{
    int index = ui->orderList->currentRow();
    if (index < 0) return;

    auto order = orders[index];

    QPrinter printer;

    QPrintDialog dialog(&printer, this);
    dialog.setWindowTitle(tr("Print report"));

    if (dialog.exec() != QDialog::Accepted) {
        return;
    }

    QPainter painter;

    painter.begin(&printer);

    int left = 60;
    int top = 80;

    painter.setFont(QFont("Consolas", 20));
    QString header1 = QString("Order for %1").arg(order.getClientName());
    painter.drawText(QPointF(left, top), header1);
    QString header2 = QString("Created on %2").arg(order.getOrderCreationTime().toString(Order::format));
    painter.drawText(QPointF(left, top + 40), header2);

    painter.setFont(QFont("Consolas", 16));

    painter.drawText(QPointF(left, top + 150), QString("").leftJustified(60, '-'));

    int i = 1;
    for (auto item : order.getAllItems())
    {
        painter.drawText(QPointF(left, top + 150 + 30 * i++), item.first.toString(item.second));
    }

    painter.drawText(QPointF(left, top + 150 + 30 * i), QString("").leftJustified(60, '-'));

    painter.drawText(QPointF(left, top + 150 + 30 * ++i), QString(" Total:%1$")
                     .arg(QString::number(order.getFullPrice(), 'f', 2).rightJustified(50)));

    painter.end();

}


