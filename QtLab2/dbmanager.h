#ifndef DBMANAGER_H
#define DBMANAGER_H

#include <QtSql>
#include "data.h"

class DbManager
{
public:
    DbManager(const QString& path);
    void ListAll();

    QList<Order> getAllOrders();
    QList<Item> getAllItems();
    void addNewItem(QString name, double price);

    QList<QPair<Item, int> > getItemsForOrder(int id);

    void addItemToOrder(int order_id, int item_id, int amount);
    void removeItemFromOrder(int order_id, int item_id);

    void addOrder(QString client);
    void removeOrder(int id);

    void updateAmount(int item_id, int order_id, int amount);
private:
    QSqlDatabase m_db;
};

#endif // DBMANAGER_H
