#ifndef ADDORDERDIALOG_H
#define ADDORDERDIALOG_H

#include <QDialog>

namespace Ui {
class AddOrderDialog;
}

class AddOrderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddOrderDialog(QWidget *parent = 0);
    ~AddOrderDialog();

    QString getName();

private:
    Ui::AddOrderDialog *ui;

    bool validate();
    void confirm();
};

#endif // ADDORDERDIALOG_H
