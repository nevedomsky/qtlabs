#include "dialog.h"
#include "ui_dialog.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    ui->setupUi(this);

    h = new PosHashTable<QString>(5);

    int x = 10;
    for (int i = -x; i <= x; i++)
    {
        h->insert(i, QString(QChar('a' + x + i)));
    }

//    h->insert(101, "a");
//    h->insert(201, "b");
//    h->insert(401, "c");
//    h->insert(501, "d");
//    h->insert(601, "e");
//    h->insert(801, "f");
//    h->insert(701, "g");
//    h->insert(300, "h");
//    h->insert(301, "i");
//    h->insert(302, "j");
//    h->insert(303, "k");
//    h->insert(304, "l");
//    h->remove(303);

//    qDebug() << h->get(300);

    refresh();

    connect(ui->addButton, &QPushButton::clicked, this, &Dialog::addPressed);
    connect(ui->removeButton, &QPushButton::clicked, this, &Dialog::removePressed);
    connect(ui->removeNegativeButton, &QPushButton::clicked, this, &Dialog::removeNegativePressed);
}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::refresh()
{
    ui->listWidget->clear();
    int i = 0;
    for (auto row : h->getEntries())
    {
        QString s = QString("[%1]:").arg(QString::number(++i));
        for (auto item : row)
        {
            s.append(" ").append(QString("{%1:%2}").arg(QString::number(item.key), item.value));
        }

        ui->listWidget->addItem(s);
    }
}

void Dialog::addPressed()
{
    h->insert(ui->keySpinBox->value(), ui->valuEditor->text());
    refresh();
}

void Dialog::removePressed()
{
    h->remove(ui->keySpinBox->value());
    refresh();
}

void Dialog::removeNegativePressed()
{
    h->removeNegative(1000);
    refresh();
}
