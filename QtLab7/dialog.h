#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include <QDebug>

#include "hashtable.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT

public:
    explicit Dialog(QWidget *parent = 0);
    ~Dialog();

private:
    Ui::Dialog *ui;
    PosHashTable<QString>* h;

    void refresh();
    void addPressed();
    void removePressed();
    void removeNegativePressed();
};

#endif // DIALOG_H
