#ifndef HASHTABLE_H
#define HASHTABLE_H

#include <QDebug>

#include <vector>
#include <cmath>

template <typename E>
struct Entry
{
    int key;
    E value;
};

template <typename E>
class HashTable
{
    int m;
    unsigned int w = 65536; //4294967296; // 2^32
    unsigned int a = 10007; //10000009; // prime 1e9 + 9
    int size = 0;

protected:
    std::vector<std::vector<Entry<E>>> arr;

private:
    int hash(int key)
    {
        int h = (int) std::floor(((a * key) % w) / (w / m));

        return h % m;
    }

    void check()
    {
        double load = 1.* size / m;

        if (load > 2.) {
            auto prev = arr;
            arr.clear();
            m *= 4;
            arr.resize(m);

            for (auto backet : prev)
            {
                for (auto entry : backet)
                {
                    insert(entry.key, entry.value);
                }
            }
        }
    }

public:

    HashTable(int m)
    {
        this->m = m;
        arr = std::vector<std::vector<Entry<E>>>(m);
    }

    int _hash(int key)
    {
        int h = (int) std::floor(((a * key) % w) / (w / m));

        return h % m;
    }

    E get(int key)
    {
        for (auto entry : arr[hash(key)])
        {
            if (entry.key == key) return entry.value;
        }

        return INT_MIN;
    }

    void insert(int key, E value)
    {
        int h = hash(key);
        for (int i = 0; i < arr[h].size(); i++)
        {
            if (arr[h][i].key == key)
            {
                arr[h][i].value = value;
                return;
            }
        }

        arr[h].push_back(Entry<E>{key, value});
        size++;
        check();
    }

    void insert(Entry<E> entry)
    {
        insert(entry.key, entry.value);
    }

    void remove(int key)
    {
        int h = hash(key);
        for (int i = 0; i < arr[h].size(); i++)
        {
            if (arr[h][i].key == key)
            {
                arr[h].erase(arr[h].begin() + i);
                size--;
                return;
            }
        }
    }
};

template <typename E>
class PosHashTable : public HashTable<E>
{
public:

    PosHashTable(int m) : HashTable(m) {}

    std::vector<std::vector<Entry<E>>> getEntries()
    {
        return arr;
    }

    void removeNegative(int count)
    {
        removeRange(-count, count);
    }

    void removeRange(int from, int count)
    {
        for (int i = 0; i < count; i++)
        {
            //qDebug() << (int)(from + i);
            remove(from + i);
        }
    }
};

#endif // HASHTABLE_H
