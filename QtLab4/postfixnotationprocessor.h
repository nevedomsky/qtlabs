#ifndef POSTFIXNOTATIONPROCESSOR_H
#define POSTFIXNOTATIONPROCESSOR_H

#include <QChar>
#include <QStack>
#include <QQueue>
#include <QString>
#include <QStringList>
#include <QDebug>

#include <algorithm>


class PostfixNotationProcessor
{
public:
    PostfixNotationProcessor();

    static QString fromInfix(QString infix)
    {
        auto out = fromInfixToQueue(infix);

        QString result;

        while (!out.empty()) result.append(out.dequeue()).append(' ');

        return result;
    }

    static QQueue<QString> fromInfixToQueue(QString infix)
    {
        infix.append(' ');
        QQueue<QString> out;
        QStack<QChar> ops;

        QString t;

        for (int i = 0; i < infix.size(); i++)
        {
            QChar c = infix.at(i);

            if (c.isNumber() || c == ',' || c == '.')
            {
                t.append(c);
            } else
            {
                if (!t.isEmpty())
                {
                    out.enqueue(t);
                }
                t.clear();
            }

            if (c.isSpace()) continue;

            if (c == '(')
            {
                ops.push(c);
                continue;
            }

            if (c == ')')
            {
                while (ops.top() != '(')
                {
                    out.enqueue(ops.pop());
                }

                ops.pop();
                continue;
            }

            if (isOperator(c))
            {
                while (!ops.empty() && ops.top().toLatin1() != '(' && getPriority(c) < getPriority(ops.top()))
                {
                    out.enqueue(ops.pop());
                }

                ops.push(c);
                continue;
            }

            if (t.isEmpty())
            {
                out.enqueue(c);
            }
        }

        while (!ops.empty())
        {
            out.enqueue(QString(ops.pop()));
        }

        return out;
    }

    static bool isOperator(QChar c)
    {
        return getPriority(c) >= 0;
    }

    static int getPriority(QChar op)
    {
        switch (op.toLatin1()) {
        case '-':
        case '+':
            return 0;
        case '*':
        case '/':
            return 1;
        case ')':
        case '(':
            return 2;
        case '^':
            return 3;
        default:
            return -1;
        }
    }
};

#endif // POSTFIXNOTATIONPROCESSOR_H
