#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QQueue>
#include <QStack>
#include <QChar>
#include <QDebug>
#include <QMap>
#include <QDoubleSpinBox>

#include "postfixnotationprocessor.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    //qDebug() << PostfixNotationProcessor::fromInfix("a + (b - c) * d"); // "3 + (4 - 8) * 2");
    //qDebug() << PostfixNotationProcessor::fromInfix("(6+1-4)/(1+1*2)+1");
    // 3 4 8 - 2 * +
    // a b c - d * +
    ui->setupUi(this);

    connect(ui->infixEditor, &QLineEdit::returnPressed, this, &MainWindow::refresh);
    connect(ui->processButton, &QPushButton::clicked, this, &MainWindow::refresh);
    connect(ui->calcButton, &QPushButton::clicked, this, &MainWindow::calculate);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::refresh()
{
    ui->calcButton->setEnabled(true);
    ui->processButton->setEnabled(false);
    vars.clear();
    QString result = PostfixNotationProcessor::fromInfix(ui->infixEditor->text());
    postfix = PostfixNotationProcessor::fromInfixToQueue(ui->infixEditor->text());
    ui->resultLabel->setText(QString("Postfix: %1").arg(result));

    ui->tableWidget->clear();
    ui->tableWidget->setRowCount(0);
    int rows = 0;
    for (int i = 0; i < result.size(); i++)
    {
        if (result[i].isLetter())
        {
            if (!vars.contains(result[i]))
            {
                vars.insert(result[i], rows);

                ui->tableWidget->insertRow(rows);
                ui->tableWidget->setItem(rows, 0, new QTableWidgetItem(QString(result[i])));

                auto spin = new QDoubleSpinBox(ui->tableWidget);
                spin->setMaximum(1e10);
                spin->setMinimum(-1e10);

                ui->tableWidget->setCellWidget(rows, 1, spin);
                rows++;
            }
        }
    }
}

void MainWindow::calculate()
{
    ui->calcButton->setEnabled(false);
    ui->processButton->setEnabled(true);
    QStack<double> result;
    QQueue<QString> copied = postfix;

    while (!copied.empty())
    {
        QString s = copied.dequeue();
        if (s[0].isLetter())
        {
            QDoubleSpinBox* b = qobject_cast<QDoubleSpinBox*>(ui->tableWidget->cellWidget(vars[s[0]], 1));
            result.push(b->value());
            continue;
        }

        if (PostfixNotationProcessor::isOperator(s[0]))
        {
            double b = result.pop();
            double a = result.pop();
            switch (s[0].toLatin1())
            {
            case '+':
                a += b;
                break;
            case '-':
                a -= b;
                break;
            case '/':
                a /= b;
                break;
            case '*':
                a *= b;
                break;
            }

            result.push(a);
            continue;
        }

        result.push(s.toDouble());
    }

    ui->resultLabel->setText(QString("Result: %1").arg(QString::number(result.pop(), 'g', 4)));
}

