#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "fireline.h"
#define M_PI 3.14159265358979323846264338327950288

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle(tr("Rocket Launcher"));
    setWindowState(Qt::WindowMaximized);

    QGraphicsScene* scene = new QGraphicsScene(this);
    //scene->setBackgroundBrush(QBrush(QColor(32, 32, 32)));

    int width = 64;
    int radius = width / 4;

    QPolygonF rocket;
    rocket.append(QPointF(0, 0));
    rocket.append(QPointF(width / 2, 100));
    rocket.append(QPointF(width / 2, 300));
    rocket.append(QPointF(width / 5 * 4, 400));
    rocket.append(QPointF(width / 4 * 3, 450));
    rocket.append(QPointF(0, 375));
    rocket.append(QPointF(-width / 4 * 3, 450));
    rocket.append(QPointF(-width / 5 * 4, 400));
    rocket.append(QPointF(-width / 2, 300));
    rocket.append(QPointF(-width / 2, 100));
    rocket.append(QPointF(0, 0));

    QPolygonF out;

    out.append(QPointF(0, 300));
    out.append(QPointF(width / 2, 350));
    out.append(QPointF(width / 6, 410));
    out.append(QPointF(-width / 6, 410));
    out.append(QPointF(-width / 2, 350));
    out.append(QPointF(0, 300));

    auto outer = scene->addPolygon(out, QPen(Qt::black), QBrush(Qt::black));
    outer->setZValue(5);

    QGraphicsPolygonItem* body = scene->addPolygon(rocket, QPen(Qt::black), QBrush(Qt::darkGray));
    body->setZValue(10);

    auto window = scene->addEllipse(-radius, 100, radius * 2, radius * 2,QPen(Qt::black), QBrush(Qt::lightGray));
    window->setZValue(15);

    ui->graphicsView->setRenderHint(QPainter::Antialiasing); //QPainter::HighQualityAntialiasing
    ui->graphicsView->setScene(scene);

    int count = 30;
    double step = M_PI / 10 / count;
    double start = M_PI / 2 - step / 2 * count;

    QList<FireLine*> lines;
    int length = 300;

    for (int i = 0; i <= count; i++)
    {
        FireLine* line = new FireLine(QPointF(0, 375), start + step * i , length);
        lines.append(line);
        line->setPen(QPen(QBrush(Qt::red), 2));
        line->setZValue(0);
        scene->addItem(line);

        auto anim = new QPropertyAnimation(line, "angle");
        anim->setStartValue(line->angle());
        anim->setEndValue(M_PI - line->angle());
        anim->setDuration(300);
        anim->setEasingCurve(QEasingCurve::Linear);
        anim->setLoopCount(1000);
        anim->start();
    }

    QTransform trans;
   // trans.rotateRadians(M_PI / 18);
    trans.rotate(20);
    ui->graphicsView->setTransform(trans);
}

MainWindow::~MainWindow()
{
    delete ui;
}
