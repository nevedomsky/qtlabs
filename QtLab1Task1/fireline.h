#ifndef FIRELINE_H
#define FIRELINE_H

#include <QObject>
#include <QGraphicsItem>

class FireLine : public QObject, public QGraphicsLineItem
{
    Q_OBJECT
    Q_PROPERTY(QPointF startpos READ startpos WRITE setStartpos)
    Q_PROPERTY(double angle READ angle WRITE setAngle)

private:
    QPointF mstartpos;
    double mangle;
    int length;
public:
    FireLine(QPointF startpos, double angle, int length);
    QPointF startpos();
    void setStartpos(QPointF pos);
    void setAngle(double angle);
    double angle();
//public:
//    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
//               QWidget *widget)
//    {
//        painter->drawRoundedRect(-10, -10, 20, 20, 5, 5);
//    }
};

#endif // FIRELINE_H
