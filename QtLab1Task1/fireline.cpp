#include "fireline.h"

FireLine::FireLine(QPointF startpos, double angle, int len)
{
    this->mangle = angle;
    this->mstartpos = startpos;
    this->length = len;
    setLine(startpos.x(), startpos.y(),
            startpos.x() + cos(angle) * len, startpos.y() + sin(angle) * len);
}


QPointF FireLine::startpos()
{
    return mstartpos;
}

void FireLine::setStartpos(QPointF pos)
{
    mstartpos = pos;
    setLine(mstartpos.x(), mstartpos.y(), mstartpos.x() + cos(mangle) * length, mstartpos.y() + sin(mangle) * length);
}

void FireLine::setAngle(double angle)
{
    this->mangle = angle;
    setLine(mstartpos.x(), mstartpos.y(), mstartpos.x() + cos(angle) * length, mstartpos.y() + sin(angle) * length);
}

double FireLine::angle()
{
    return mangle;
}
