#ifndef ADDABITURIENTDIALOG_H
#define ADDABITURIENTDIALOG_H

#include <QDialog>

#include "abiturient.h"
#include "list.h"

namespace Ui {
class AddAbiturientDialog;
}

class AddAbiturientDialog : public QDialog
{
    Q_OBJECT

    Abiturient* a;
    List<Mark> m;

public:
    explicit AddAbiturientDialog(QWidget *parent = 0, Abiturient* a = nullptr);
    ~AddAbiturientDialog();

    Abiturient* getData() { return a; }

private:
    Ui::AddAbiturientDialog *ui;
    void confirm();
    void refreshMarksList();
    void addMarkClicked();
    void removeMarkClicked();
};

#endif // ADDABITURIENTDIALOG_H
