#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QToolButton>
#include <QDoubleSpinBox>

#include "list.h"
#include "abiturient.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

private:
    List<Abiturient*> abiturients;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void addPersonClicked();
    void refreshAbiturientList();
    void removePersonClicked();
    void showAbiturientInfo();
};

#endif // MAINWINDOW_H
