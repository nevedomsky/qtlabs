#include "mainwindow.h"
#include "ui_mainwindow.h"

#include "addabiturientdialog.h"

#include <algorithm>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

//    List<int> li{};

//    li.push_back(1);
//    li.push_back(2);
//    li.push_back(3);
//    li.push_back(4);
//    li.push_back(5);

//    auto it = li.iterator();

//    while (it.hasNext())
//        qDebug() << it.next();

//    qDebug() << "";
//    li.removeAt(3);
//    li.removeAt(3);
//    li.removeAt(0);
//    li.push_front(0);
//    li.push_back(100);

//    it = li.iterator();

//    while (it.hasNext())
//        qDebug() << it.next();

    abiturients = List<Abiturient*>{};

    refreshAbiturientList();

    connect(ui->addPersonButton, &QPushButton::clicked, this, &MainWindow::addPersonClicked);
    connect(ui->removePersonButton, &QPushButton::clicked, this, &MainWindow::removePersonClicked);
    connect(ui->searchEditor, &QLineEdit::textChanged, this, &MainWindow::refreshAbiturientList);
    connect(ui->clearSearchButton, &QToolButton::clicked, [=] { ui->searchEditor->clear(); });
    connect(ui->gradeSelector, QOverload<double>::of(&QDoubleSpinBox::valueChanged), [=] { refreshAbiturientList(); });
    connect(ui->maximumSelector, QOverload<int>::of(&QSpinBox::valueChanged), [=] { refreshAbiturientList(); });
    connect(ui->abiturientList, &QListWidget::activated, [=] { showAbiturientInfo(); });
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::addPersonClicked()
{
    AddAbiturientDialog d(this);
    if (d.exec())
    {
        abiturients.push_back(d.getData());
        refreshAbiturientList();
    }
}

void MainWindow::removePersonClicked()
{
    int index = ui->abiturientList->currentRow();
    if (index < 0) return;

    abiturients.removeAt(index);
    refreshAbiturientList();
}

void MainWindow::refreshAbiturientList()
{
    int all = ui->maximumSelector->value();
    int taken = abiturients.size();

    ui->removePersonButton->setEnabled(!abiturients.empty());
    ui->addPersonButton->setEnabled(taken < all);

    ui->abiturientList->clear();

    auto it = abiturients.iterator();

    while (it.hasNext())
    {
        auto a = it.next();
        if (a->name().contains(ui->searchEditor->text(), Qt::CaseInsensitive) &&
                a->getAverageMark() >= ui->gradeSelector->value())
        {
            ui->abiturientList->addItem(a->toString());
        }
    }

    int free = std::max(all - taken, 0);

    QString s = QString("Abiturients (%1 of %2, %3 free, %4 is left)")
            .arg(QString::number(taken))
            .arg(QString::number(all))
            .arg(QString::number(free))
            .arg(QString::number(std::abs(all - taken - free)));

    ui->abiturientsLabel->setText(s);
}

void MainWindow::showAbiturientInfo()
{
    int index = ui->abiturientList->currentRow();
    if (index < 0) return;

    AddAbiturientDialog d(this, abiturients.at(index));

    if (d.exec())
    {
        //abiturients.at(index) = d.getData();
        refreshAbiturientList();
    }
}
