#include "addabiturientdialog.h"
#include "ui_addabiturientdialog.h"

#include "addmarkdialog.h"

#include <QMessageBox>

AddAbiturientDialog::AddAbiturientDialog(QWidget *parent, Abiturient* abit) :
    QDialog(parent),
    ui(new Ui::AddAbiturientDialog),
    a(abit)
{
    ui->setupUi(this);

    if (a != nullptr)
    {
        m = a->marks();
        ui->nameEditor->setText(a->name());
    }
    else m = List<Mark>{};

    refreshMarksList();

    connect(ui->acceptButton, &QPushButton::clicked, this, &AddAbiturientDialog::confirm);
    connect(ui->addMarkButton, &QPushButton::clicked, this, &AddAbiturientDialog::addMarkClicked);
    connect(ui->removeMarkButton, &QPushButton::clicked, this, &AddAbiturientDialog::removeMarkClicked);
}

AddAbiturientDialog::~AddAbiturientDialog()
{
    delete ui;
}

void AddAbiturientDialog::confirm()
{
    if (ui->nameEditor->text().isEmpty())
    {
        QMessageBox::warning(this, tr("Invalid format"), tr("Name field is empty"));
        return;
    }

    if (m.empty())
    {
        QMessageBox::warning(this, tr("Invalid format"), tr("No marks added"));
        return;
    }

    a = new Abiturient(ui->nameEditor->text());

    auto it = m.iterator();

    while (it.hasNext())
    {
        a->addMark(it.next());
    }

    accept();
}

void AddAbiturientDialog::refreshMarksList()
{
    ui->markList->clear();

    double sum = 0.;
    auto it = m.iterator();

    while (it.hasNext())
    {
        auto mark = it.next();
        ui->markList->addItem(mark.toString());
        sum += mark.mark();
    }

    double average = 0.;
    if (!m.empty()) average = sum / m.size();

    ui->averageLable->setText(QString("Average mark: %1").arg(QString::number(average)));
}

void AddAbiturientDialog::addMarkClicked()
{
    AddMarkDialog d{this};
    if (d.exec())
    {
        m.push_back(d.getData());
        refreshMarksList();
    }
}

void AddAbiturientDialog::removeMarkClicked()
{
    int index = ui->markList->currentRow();
    if (index < 0) return;

    m.removeAt(index);
    refreshMarksList();
}
