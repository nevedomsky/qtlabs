#ifndef ADDMARKDIALOG_H
#define ADDMARKDIALOG_H

#include <QDialog>

#include "abiturient.h"

namespace Ui {
class AddMarkDialog;
}

class AddMarkDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddMarkDialog(QWidget *parent = 0);
    ~AddMarkDialog();

    Mark getData();

private:
    Ui::AddMarkDialog *ui;
    void confirm();
};

#endif // ADDMARKDIALOG_H
