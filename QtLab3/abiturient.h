#ifndef ABITURIENT_H
#define ABITURIENT_H

#include "list.h"

#include <QString>

class Mark
{
    QString m_subject;
    double m_mark;

public:
    Mark(QString subject, double mark) : m_subject(subject), m_mark(mark) {}

    double mark() { return m_mark; }

    QString subject() { return m_subject; }

    QString toString()
    {
        auto s = QString("%1 | %2").arg(
                    m_subject.leftJustified(30),
                    QString::number(m_mark, 'f', 2)
        );

        return s;
    }
};

class Abiturient
{
    QString m_name;
    List<Mark> m_marks;
public:
    Abiturient(QString name) : m_name(name)
    {
        m_marks = List<Mark>{};
    }

    void addMark(Mark mark)
    {
        m_marks.push_back(mark);
    }

    QString name() { return m_name; }

    List<Mark> marks() { return m_marks; }

    double getAverageMark()
    {
        if (m_marks.empty()) return 0.;

        double sum = 0.;
        auto it = m_marks.iterator();

        while (it.hasNext()) sum += it.next().mark();

        return sum / m_marks.size();
    }

    QString toString()
    {
        auto s = QString("%1 | %2").arg(
                    m_name.leftJustified(80),
                    QString::number(getAverageMark(), 'f', 2)
        );

        return s;
    }
};

#endif // ABITURIENT_H
