#include "addmarkdialog.h"
#include "ui_addmarkdialog.h"

#include <QMessageBox>

AddMarkDialog::AddMarkDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddMarkDialog)
{
    ui->setupUi(this);
    connect(ui->acceptButton, &QPushButton::clicked, this, &AddMarkDialog::confirm);
}

AddMarkDialog::~AddMarkDialog()
{
    delete ui;
}

void AddMarkDialog::confirm()
{
    if (ui->subjectEditor->text().isEmpty())
    {
        QMessageBox::warning(this, tr("Invalid format"), tr("Name field is empty"));
        return;
    }

    accept();
}

Mark AddMarkDialog::getData()
{
    return Mark{ui->subjectEditor->text(), ui->markSelector->value()};
}
